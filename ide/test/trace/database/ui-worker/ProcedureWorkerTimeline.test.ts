/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

jest.mock('../../../../src/trace/component/trace/base/TraceRow', () => {
  return {};
});
jest.mock('../../../../src/js-heap/model/DatabaseStruct', () => {
  return {};
});
import {
  RangeRuler,
  SportRuler,
  timeline,
  TimelineRender,
} from '../../../../src/trace/database/ui-worker/ProcedureWorkerTimeline';
import { Rect } from '../../../../src/trace/component/trace/timer-shaft/Rect';
jest.mock('../../../../src/trace/database/ui-worker/ProcedureWorkerSnapshot', () => {
  return {};
});
jest.mock('../../../../src/trace/database/ui-worker/ProcedureWorker', () => {
    return {}; });

describe(' ProcedureWorkerTimelineTest', () => {
  it('timelineTest', () => {
    const timelineCanvas = document.createElement('canvas');
    timelineCanvas.width = 12;
    timelineCanvas.height = 12;
    const ctx = timelineCanvas.getContext('2d');
    let dataList = new Array();
    dataList.push({
      startTime: 0,
      dur: 120,
      frame: { x: 0, y: 9, width: 120, height: 10 },
    });
    dataList.push({ startTime: 1, dur: 111 });
    let rect = new Rect(0, 10, 10, 10);
    let keyboardEvent: KeyboardEvent = new KeyboardEvent('w', <KeyboardEventInit>{ ctrlKey: true, keyCode: 13 });
    timeline(timelineCanvas, ctx, 1, 100254, keyboardEvent, rect, null, null, null, null, null, null, 0, 0, (e: any) => {});
  });

    it('SportRulerTest01', () => {
        const canvas = document.createElement('canvas');
        canvas.width = 1;
        canvas.height = 1;
        const ctx = canvas.getContext('2d');
        let rect = new Rect(0, 10, 10, 10);
        let sportRuler = new SportRuler(canvas, ctx, rect);
        sportRuler.modifyFlagList('amend');
        sportRuler.modifyFlagList('remove');
        sportRuler.drawTheFlag(0, '#999999', false, '');
        sportRuler.randomRgbColor();
        sportRuler.mouseMove(new MouseEvent(''));
        sportRuler.mouseUp(new MouseEvent(''));
        sportRuler.onFlagRangeEvent('1', 2);
    });

    it('SportRulerTest02', () => {
        const canvas = document.createElement('canvas');
        canvas.width = 2;
        canvas.height = 1;
        const ctx = canvas.getContext('2d');
        let rect = new Rect(0, 20, 10, 20);
        let rangeRuler = new RangeRuler(
            canvas,
            ctx,
            rect,
            {
                startX: 10,
                endX: rect.width,
                startNS: 40,
                endNS: 14,
                totalNS: 155,
                xs: [],
                xsTxt: [],
            },
            (a: any) => {}
        );
        rangeRuler.draw();
        rangeRuler.drawCpuUsage();
        rangeRuler.mouseDown({ offsetX: 1, offsetY: 1 });
        rangeRuler.mouseUp(new MouseEvent(''));
        rangeRuler.mouseMove(new MouseEvent(''));
        rangeRuler.mouseOut(new MouseEvent(''));
        rangeRuler.range.startNS = -2;
        rangeRuler.range.endNS = -2;
        rangeRuler.range.totalNS = -7;
        rangeRuler.fillX();
        rangeRuler.keyPress(new KeyboardEvent(''));
        rangeRuler.pressFrameId != -1;
        rangeRuler.keyUp(new KeyboardEvent(''));
        rangeRuler.keyUp({ key: 'w' });
        rangeRuler.keyUp({ key: 's' });
        rangeRuler.keyUp({ key: 'a' });
        rangeRuler.keyUp({ key: 'd' });
    });

  it('SportRulerTest03', () => {
    const canvas = document.createElement('canvas');
    canvas.width = 1;
    canvas.height = 4;
    const ctx = canvas.getContext('2d');
    let rect = new Rect(0, 50, 10, 50);
    let rangeRuler = new RangeRuler(
      canvas,
      ctx,
      rect,
      {
        startX: 60,
        endX: rect.width,
        startNS: 660,
        endNS: 116,
        totalNS: 661,
        xs: [],
        xsTxt: [],
      },
      (a: any) => {}
    );
    rangeRuler.cpuUsage = true;
    expect(rangeRuler.cpuUsage).toBeUndefined();
  });
});
