/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { perfDataQuery } from '../../../../src/trace/component/chart/PerfDataQuery';

jest.mock('../../../../src/trace/component/chart/SpHiPerf', () => {
  return true;
});
jest.mock('../../../../src/trace/component/trace/base/TraceRow', () => {
  return {}
});

describe('perfDataQuery Test', () => {
  it('perfDataQueryTest01 ', function () {
    expect(perfDataQuery.initPerfFiles).not.toBeUndefined();
  });
});
